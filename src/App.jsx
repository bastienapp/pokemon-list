import { useState } from "react";
import "./App.css";
import Pokedex from "./components/Pokedex";

function App() {
  const [showPokedex, setShowPokedex] = useState(true);
  return (
    <>
      { showPokedex ? <Pokedex /> : null}
      <button onClick={() => setShowPokedex(!showPokedex)}>
        Afficher/cacher le pokédex
      </button>
    </>
  );
}

export default App;
