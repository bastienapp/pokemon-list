import { useEffect, useState } from "react";
import axios from "axios";

// rfce : react funtional component export
function Pokedex() {
  const [pokedexName, setPokedexName] = useState("Chargement...");

  // cycle de vie d'un composant
  // - montage du composant : il a été créé
  // - mise à jour du composant : modification du state (au moins une fois, après le montage du composant)
  // - démontage (destruction) du composant

  // il sert à atteindre des étapes du cycles
  useEffect(() => {
    console.log("montage du composant");
    // faire un appel d'API pour récupérer la liste des pokémons
    axios
      .get("https://pokeapi.co/api/v2/pokedex/2")
      .then((response) => {
        console.log("appel d'API terminé");
        // axios stocker le contenu du json dans une propriété .data
        const pokedex = response.data;
        setPokedexName(capitalize(pokedex.name));
      });
  }, []); // on ajoute [] pour atteindre le montage du composant uniquement (et pas lors d'une modification du state)

  useEffect(() => {
    return () => {
      console.log("démontage du composant");
    };
  }, []);

  useEffect(() => {
    console.log("mise à jour du composant le state pokedexName");
  }, [pokedexName]);

  function capitalize(string) {
    return string[0].toUpperCase() + string.slice(1);
  }

  return (
    <div>
      <h3>{pokedexName}</h3>
    </div>
  );
}

export default Pokedex;
